//
//  CustomCellTableViewCell.swift
//  AssignmentForMQT
//
//  Created by Admin on 13/06/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CustomCellTableViewCell: UITableViewCell {

    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureTableViewCell(withdataDict:NSDictionary)
    {
        
        self.authorLbl.text = withdataDict["author"] as! String?
        
        self.titleLbl.text = withdataDict["title"] as! String?
        
        
        if let imageUrlString = withdataDict["thumbnail"] as? String
        {
            
            self.avatarImage.downloadedFrom(link: imageUrlString)
        }
        else
        {
            
            let image = UIImage(named:"avatarImage")
            self.avatarImage.image = image
            
        }
        
        self.avatarImage.layer.cornerRadius = 8.0
        self.avatarImage.clipsToBounds = true
        
    }
    
    

}

