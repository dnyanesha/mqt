//
//  Webservice.swift
//  AssignmentForMQT
//
//  Created by Admin on 13/06/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

final class Webservice
{
    func load(resource: Resource, completion: @escaping (Any?) -> ()) {
        URLSession.shared.dataTask(with: resource.url as URL) { data, _, error in
            guard let data = data else {
                completion(nil)//Todo error handling - network error
                return
            }
            completion(resource.parse(data))
            }.resume()
    }
}

