//
//  WebResouces.swift
//  AssignmentForMQT
//
//  Created by Admin on 13/06/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

typealias JSONArray = [String:Any]

let user_url = NSURL(string: "http://blog.teamtreehouse.com/api/get_recent_summary/")!



struct Resource {
    let url: NSURL
    let parse: (Data) -> (Any)?
    //    let errorHandler:(Error) ->()? // Error
}

extension Resource {
    init(url : NSURL, parseJSON: @escaping (Any) -> Any?){
        
        self.url = url as NSURL
        self.parse = {
            data in
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            return parseJSON (json!)
        }
    }
}

class UsersResource {
    
    static let userData = Resource(url: user_url, parseJSON: { json in
        guard let arr = json as? JSONArray else { return nil }
        return json
    })
}
