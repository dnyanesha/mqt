//
//  ViewController.swift
//  AssignmentForMQT
//
//  Created by Admin on 13/06/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableViewAuthor: UITableView!
    var refreshControl: UIRefreshControl!
   
    var posts : NSMutableArray = []
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (posts.count)
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCellTableViewCell
        let dict = posts[indexPath.row] as! [String : Any]
        cell.configureTableViewCell(withdataDict: dict as NSDictionary)
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableViewAuthor.addSubview(refreshControl)
        
        //Calculate
        self.tableViewAuthor.estimatedRowHeight = 118
        self.tableViewAuthor.rowHeight = UITableViewAutomaticDimension
        
        //Web API call
        self.callWebService();
        
        
    }
    
    
    func callWebService()
    {
        
        Webservice().load(resource: UsersResource.userData) { result in
            guard let resultSet = result as? [String:Any] else { return }
            // print("json data is : \(resultSet)")
            let tempArray = resultSet["posts"] as? NSArray
            self.posts.removeAllObjects()
            self.posts = tempArray?.mutableCopy() as! NSMutableArray
            DispatchQueue.main.async{
                self.refreshControl.endRefreshing()
                
                self.reloadTableViewData()
                
            }
            
            
        }

        
    }
    
    func refresh() {
        //  your code to refresh tableView here we require to hit web API
        self.callWebService()
        
    }
    
    func reloadTableViewData()
    {
        self.tableViewAuthor.reloadData()
        print("myPosts is : \(self.posts)")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

